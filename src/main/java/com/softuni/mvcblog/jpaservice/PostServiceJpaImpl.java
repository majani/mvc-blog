package com.softuni.mvcblog.jpaservice;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.softuni.mvcblog.models.Post;
import com.softuni.mvcblog.repositories.PostRepository;
import com.softuni.mvcblog.service.PostService;

@Service
@Primary
public class PostServiceJpaImpl implements PostService {

	@Autowired
	PostRepository postRepo;

	@Override
	public List<Post> findAll() {

		return postRepo.findAll();
	}

	@Override
	public List<Post> findLatest5() {
		return this.postRepo.findLatest5Posts().stream().limit(5).collect(Collectors.toList());
	}

	@Override
	public Post findById(Long id) {
		return postRepo.findById(id).get();
	}

	@Override
	public Post create(Post post) {
		return postRepo.save(post);
	}

	@Override
	public Post edit(Post post) {
		return postRepo.save(post);
	}

	@Override
	public void deleteById(Long id) {

		Post post = postRepo.findById(id).get();
		postRepo.delete(post);
	}

	@Override
	public List<Post> findMyPosts(Long id) {
		return postRepo.findByAuthorId(id);
	}
	

}
