package com.softuni.mvcblog.jpaservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.softuni.mvcblog.models.User;
import com.softuni.mvcblog.repositories.UserRepository;
import com.softuni.mvcblog.service.UserService;

@Service
@Primary
public class UserServiceJpaImpl implements UserService {

	@Autowired
	UserRepository userRepo;

	@Override
	public List<User> findAll() {

		return userRepo.findAll();
	}

	@Override
	public User findById(Long id) {
		return userRepo.findById(id).get();
	}

	@Override
	public User create(User user) {
		return userRepo.save(user);
	}

	@Override
	public User edit(User user) {
		return userRepo.save(user);
	}

	@Override
	public void deleteById(Long id) {

		userRepo.findById(id).get();
	}

}
