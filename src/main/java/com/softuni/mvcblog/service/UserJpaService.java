package com.softuni.mvcblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.softuni.mvcblog.models.User;
import com.softuni.mvcblog.repositories.UserRepository;

@Service
public class UserJpaService implements UserDetailsService {

	@Autowired
	UserRepository userRepo;

	public UserJpaService(UserRepository userRepo) {
		super();
		this.userRepo = userRepo;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByUsername(username);

		if (user != null) {
			return user;
		}

		throw new UsernameNotFoundException("User " + username + " was not found");
	}

}
