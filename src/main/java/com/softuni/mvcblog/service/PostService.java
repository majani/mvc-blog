package com.softuni.mvcblog.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.softuni.mvcblog.models.Post;
@Service
public interface PostService {

	List<Post> findAll();

	List<Post> findLatest5();

	Post findById(Long id);

	Post create(Post post);

	Post edit(Post post);
	
	List<Post> findMyPosts(Long id);

	void deleteById(Long id);
}
