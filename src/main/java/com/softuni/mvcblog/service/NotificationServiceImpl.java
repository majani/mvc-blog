package com.softuni.mvcblog.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softuni.mvcblog.service.NotificationMessage.NotificationMessageType;

@Service
public class NotificationServiceImpl implements NotificationService {

	public static final String NOTIFY_MSG_SESSION_KEY = "siteNotificationMessages";

	@Autowired
	private HttpSession httpSession;

	private void addNotificationMessage(NotificationMessageType type, String msg) {
		@SuppressWarnings("unchecked")
		List<NotificationMessage> notifyMessages = (ArrayList<NotificationMessage>) httpSession.getAttribute(NOTIFY_MSG_SESSION_KEY);
		if (notifyMessages == null) {
			notifyMessages = new ArrayList<NotificationMessage>();
		}
		notifyMessages.add(new NotificationMessage(type, msg));
		httpSession.setAttribute(NOTIFY_MSG_SESSION_KEY, notifyMessages);
	}

	@Override
	public void addInfoMessage(String msg) {

		addNotificationMessage(NotificationMessageType.INFO, msg);
	}

	@Override 
	public void addErrorMessage(String msg) {

		addNotificationMessage(NotificationMessageType.ERROR, msg);
	}

	
	

}
