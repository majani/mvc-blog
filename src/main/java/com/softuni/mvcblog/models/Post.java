package com.softuni.mvcblog.models;

import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Type;


import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@RequiredArgsConstructor
@Table(name="posts")
public class Post {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private final Long id;
	@NotBlank(message="Title is required")
	private final String title;
	@NotBlank(message="Please write content for blog")
	@Type(type = "org.hibernate.type.TextType")
	private final String body;
	@ManyToOne(optional = false)
	private final User author;
	private final Date date = new Date();
	
	
	
	
}
