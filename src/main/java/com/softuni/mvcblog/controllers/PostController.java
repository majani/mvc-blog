package com.softuni.mvcblog.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.softuni.mvcblog.forms.PostForm;
import com.softuni.mvcblog.models.Post;
import com.softuni.mvcblog.models.User;
import com.softuni.mvcblog.service.NotificationService;
import com.softuni.mvcblog.service.PostService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/posts")
public class PostController {

	@Autowired
	private PostService postService;

	@Autowired
	private NotificationService notifyService;

	@GetMapping("/view/{id}")
	public String view(@PathVariable Long id, Model model) {
		Post post = postService.findById(id);
		if (post == null) {
			notifyService.addErrorMessage("Cannot find post #" + id);
			return "redirect:/";
		}

		model.addAttribute("post", post);
		log.info("RENDERING THE BLOG TO VIEW");

		return "posts/view";

	}
	
	@ModelAttribute(name = "post")
	public Post post() {
		return new Post();
	}

	@GetMapping("/create")
	public String showPostForm() {
		log.info("RENEDERING BLOG CREATION PAGE");
		return "/posts/create";
	}

	@PostMapping
	public String createPost(@Valid PostForm form, BindingResult result, @AuthenticationPrincipal User user) {
		if (result.hasErrors()) {
			return "create";
		}
		log.info("CREATING THE BLOG");
		postService.create(form.toPost(user));
		return "redirect:/";

	}

	@GetMapping
	public String listPosts(Model model) {
		List<Post> posts = postService.findAll();
		model.addAttribute("posts", posts);
		log.info("LISTING THE POSTS");
		return "posts/list";

	}

	@GetMapping("/mine")
	public String myPostsPage(Model model, @AuthenticationPrincipal User user) {
		Long id = user.getId();
		String name = user.getFullName();
		List<Post> posts = postService.findMyPosts(id);
		model.addAttribute("posts", posts);
		log.info("LISTING USER " + name + " POSTS");

		return "posts/list";
	}
}
