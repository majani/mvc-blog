package com.softuni.mvcblog.forms;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.softuni.mvcblog.models.User;

import lombok.Data;

@Data
public class RegistrationForm {
	private final String username;
	private final String password;
	private final String fullName;

	public User toUser(BCryptPasswordEncoder passwordEncoder) {
		return new User(0L,username, passwordEncoder.encode(password), fullName);

	}

} 
