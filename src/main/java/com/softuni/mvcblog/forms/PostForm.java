package com.softuni.mvcblog.forms;

import java.util.Date;

import javax.persistence.ManyToOne;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

import com.softuni.mvcblog.models.Post;
import com.softuni.mvcblog.models.User;

import lombok.Data;

@Data
public class PostForm {
	
	private final String title;
	private final String body;
	private final User author;
	private final Date date = new Date();
	
	
	public Post toPost(@AuthenticationPrincipal User user ) {
		
		return new Post(0L, title,body, user);
	}
	

}
