package com.softuni.mvcblog.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.softuni.mvcblog.models.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

	@Query("Select p from Post p left join fetch p.author order by p.date desc")
	List<Post> findLatest5Posts();

	List<Post> findByAuthorId(Long id);
}
